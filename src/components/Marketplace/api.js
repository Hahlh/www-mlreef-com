const APP_HOST = process.env.GATSBY_APP_HOST || 'http://ec2-3-126-88-77.eu-central-1.compute.amazonaws.com:10080';

const endpoint = `${APP_HOST}/api/v4/projects`;
const mockedEndpoint = '/mocks/marketplace.json';

const createParams = () => ({
  headers: { 'PRIVATE-TOKEN': 'cgTV1etLTJXp8GyDF2Kz' },
});

export const fetchProjects = (params = {}) => {
  const url = new URL(endpoint);
  // set query params
  Object.entries({ simple: true, ...params })
    .forEach((param) => url.searchParams.append(...param));

  return fetch(mockedEndpoint, createParams())
    .then((res) => res.json());
  // .then((projects) => Array.isArray(projects)
  //   ? projects
  //   : Promise.reject(projects));
};

export const fetchProjectContributors = projectId => {
  const url = new URL(`${APP_HOST}/api/v4/projects/${projectId}/members`);

  return fetch(url, createParams());
};
