import React, { useEffect, useState } from 'react';
import './Marketplace.scss';
import { fetchProjects } from './api';
import MarketplaceCard from './MarketplaceCard';
import HomeStatement from '../HomeStatement';
import MTabs from '../../../externals/components/MTabs';

const initialRepos = {
  projects: [],
  models: [],
  dataOps: [],
  dataVisualization: [],
};

const Marketplace = () => {
  const [repos, setRepos] = useState(initialRepos);

  useEffect(() => {
    fetchProjects()
      // .then(res => console.log(res) || res)
      .then(setRepos);
  }, []);

  return (
    <div className="marketplace d-none d-lg-block">
      <HomeStatement
        title="A reef for a ML community"
        text="Access community repositories with data sets, models, data operations and visualizations and reuse them to create the most satisfying ML experience!"
      />
      <div className="mx-auto">
        <MTabs>
          <MTabs.Section
            defaultActive
            id="MLProjects"
            label="ML Projects"
            color="#91BD44"
          >
            <div className="marketplace-header">
              a preview of some of the most popular ML projects
            </div>
            <div className="marketplace-content">
              {repos.projects.map(proj => (
                <MarketplaceCard
                  key={`project-card-${proj.id}`}
                  push={() => {}}
                  owner={proj.id}
                  title={proj.name}
                  projectId={proj.id}
                  branch={proj.default_branch}
                  description={proj.description}
                  starCount={proj.star_count}
                  forkCount={proj.forks_count}
                  namespace={proj.path_with_namespace.split('/')[0]}
                  updatedAt={proj.last_activity_at}
                  handleShowModal={() => {}}
                  types={proj.tag_list}
                  users={proj.users}
                  projects={repos.projects}
                />
              ))}
            </div>
          </MTabs.Section>

          <MTabs.Section
            id="models"
            label="Models"
            color="#E99444"
          >
            <div className="marketplace-header">
              a preview of some of the most popular ML projects
            </div>

            <div className="marketplace-content">
              {repos.models.map(proj => (
                <MarketplaceCard
                  key={`project-card-${proj.id}`}
                  push={() => {}}
                  owner={proj.id}
                  title={proj.name}
                  projectId={proj.id}
                  branch={proj.default_branch}
                  description={proj.description}
                  starCount={proj.star_count}
                  forkCount={proj.forks_count}
                  namespace={proj.path_with_namespace.split('/')[0]}
                  updatedAt={proj.last_activity_at}
                  handleShowModal={() => {}}
                  types={proj.tag_list}
                  users={proj.users}
                  projects={repos.models}
                />
              ))}
            </div>
          </MTabs.Section>

          <MTabs.Section
            id="dataOps"
            label="Data Operations"
            color="#D2519D"
          >
            <div className="marketplace-header">
              a preview of some of the most popular ML projects
            </div>

            <div className="marketplace-content">
              {repos.dataOps.map(proj => (
                <MarketplaceCard
                  key={`project-card-${proj.id}`}
                  push={() => {}}
                  owner={proj.id}
                  title={proj.name}
                  projectId={proj.id}
                  branch={proj.default_branch}
                  description={proj.description}
                  starCount={proj.star_count}
                  forkCount={proj.forks_count}
                  namespace={proj.path_with_namespace.split('/')[0]}
                  updatedAt={proj.last_activity_at}
                  handleShowModal={() => {}}
                  types={proj.tag_list}
                  users={proj.users}
                  projects={repos.dataOps}
                />
              ))}
            </div>
          </MTabs.Section>

          <MTabs.Section
            id="dataVisualization"
            label="Data Visualisations"
            color="#735DA8"
          >
            <div className="marketplace-header">
              a preview of some of the most popular ML projects
            </div>

            <div className="marketplace-content">
              {repos.dataVisualization.map(proj => (
                <MarketplaceCard
                  key={`project-card-${proj.id}`}
                  push={() => {}}
                  owner={proj.id}
                  title={proj.name}
                  projectId={proj.id}
                  branch={proj.default_branch}
                  description={proj.description}
                  starCount={proj.star_count}
                  forkCount={proj.forks_count}
                  namespace={proj.path_with_namespace.split('/')[0]}
                  updatedAt={proj.last_activity_at}
                  handleShowModal={() => {}}
                  types={proj.tag_list}
                  users={proj.users}
                  projects={repos.dataVisualization}
                />
              ))}
            </div>
          </MTabs.Section>
        </MTabs>
      </div>
    </div>
  );
};

export default Marketplace;
