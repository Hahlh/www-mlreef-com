import React from 'react';
import './HomeFeatures.scss';

/**
* @param {Array[Object]} props.features required.
* @param {String} props.features[{title}] required.
* @param {String} props.features[{text}] required.
* @param {String} props.features[{image}] image's url.
 */
const HomeFeatures = props => {
  const { features } = props;

  return (
    <section className="home-features">
      {features.map(fea => (
        <div key={fea.title} className="home-features-item mx-auto">
          {fea.image && (
            <div className="home-features-item-image my-3 mx-auto">
              <img src={fea.image} alt={fea.title} />
            </div>
          )}
          <h5 className="home-features-item-title my-3">{fea.title}</h5>
          <p className="home-features-item-text my-3">{fea.text}</p>
        </div>
      ))}
    </section>
  );
};

export default HomeFeatures;
