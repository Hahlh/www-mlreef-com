import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';
// import { useLocation } from '@reach/router';

const SEO = props => {
  const { node, isPost } = props;

  const { site: { siteMetadata: site } } = useStaticQuery(
    graphql`
      query FetchSiteMetadata {
        site {
          siteMetadata {
            title
            url
            description
            keywords
            logo
          }
        }
      }
    `,
  );

  // this doesn't work in SSR
  // const { origin, href: url } = useLocation();
  const title = isPost ? node.title : site.title;
  const description = isPost ? node.description : site.description;
  const image = site.url + (isPost ? node.image : site.logo);
  const { keywords } = site;

  const schemaOrgJSONLD = [
    {
      '@context': 'http://schema.org',
      '@type': 'WebSite',
      url: site.url,
      name: title,
      alternateName: site.titleAlt ? site.titleAlt : '',
    },
  ];

  return (
    <Helmet>
      {/* General tags */}
      <meta name="description" content={description} />
      <meta name="image" content={image} />
      <meta name="keywords" content={keywords} />

      {/* Schema.org tags */}
      <script type="application/ld+json">
        {JSON.stringify(schemaOrgJSONLD)}
      </script>

      {/* OpenGraph tags */}
      <meta property="og:url" content={site.url} />
      {isPost && <meta property="og:type" content="article" />}
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      {site.fbAppId && <meta property="fb:app_id" content={site.fbAppId} />}

      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary_large_image" />
      {site.userTwitter && <meta name="twitter:creator" content={site.userTwitter} />}
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
    </Helmet>
  );
};

export default SEO;
