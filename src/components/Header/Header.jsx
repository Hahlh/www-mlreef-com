import React, { useEffect, useState } from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import MLink from '../MLink';
import './Header.scss';
import HeaderMainMenu from './HeaderMainMenu';
import MDropdown from '../../../externals/components/MDropdown';

// const logoDark = '/images/MLReef_Logo_POS_H-01.svg';
// const logoLight = '/images/MLReef_Logo_Neg_H-01-light.svg';
const logoDark = '/images/MLReef_Logo_Neg_H_ALPHA-01.svg';
const logoLight = '/images/MLReef_Logo_POS_H_ALPHA-01.svg';

const Header = () => {
  const [isOnTop, setIsOnTop] = useState(true);
  const [menuShown, setMenuShown] = useState(false);

  const {
    site: { siteMetadata: { mlreefUrl } },
    dataYaml: { header: data },
  } = useStaticQuery(
    graphql`
    query FetchHeaderData {
      site {
        siteMetadata {
          mlreefUrl
        }
      }

      dataYaml {
        header {
          links {
            label
            href
            menu {
              title
              items {
                label
                subtext
                color
                href
              }
            }
          }
        }
      }
    }
    `,
  );

  // console.log(data);

  const darkNavbarOnTop = () => {
    if (window) {
      setIsOnTop(!window.scrollY);
    }
  };

  useEffect(
    () => {
      if (window) {
        window.addEventListener('scroll', darkNavbarOnTop);
      }

      return () => {
        if (window) {
          window.removeEventListener('scroll', darkNavbarOnTop);
        }
      };
    },
    [],
  );

  const renderDropdown = ({ label, menu }) => (
    <MDropdown
      key={`header-menu-${label}`}
      label={label}
      buttonClasses="btn btn-dark px-3"
      component={(
        <div className="header-menu-content">
          <div className="header-menu-content-title">{menu.title}</div>
          <ul className="header-menu-content-list">
            {menu.items.map(item => (
              <li
                key={`menu-${label}-${item.label}`}
                className="header-menu-content-list-item"
              >
                <MLink to={item.href} className="nav-link">
                  <div style={{ color: item.color }} className="label">
                    {item.label}
                  </div>
                  <div className="subtext">
                    {item.subtext}
                  </div>
                </MLink>
              </li>
            ))}
          </ul>
        </div>
      )}
    />
  );

  const renderLink = ({ label, href }) => (
    <MLink
      key={`header-link-${label}`}
      to={href}
      className="header-links-item btn btn-dark nav-link px-3 ml-3"
    >
      {label}
    </MLink>
  );

  return (
    <header className={`header ${isOnTop ? '' : 'light'}`}>
      <div className="header-section left flex-0">
        <MLink to="/">
          <img
            alt="mlreef brand"
            className="header-brand m-auto"
            src={isOnTop ? logoDark : logoLight}
          />
        </MLink>
      </div>
      <div className="header-section center">
        <div className="header-links d-none d-lg-flex">
          {data.links.map(attrs => attrs.menu ? renderDropdown(attrs) : renderLink(attrs))}
        </div>
      </div>
      <div className="header-section right">
        <div className="header-account-links d-none d-lg-flex">
          <a href={mlreefUrl} className="btn btn-dark mr-3 px-3">
            Sign in
          </a>
          <MLink to="https://mlreef.com/register" className="btn btn-outline-secondary keep-border">
            Register
          </MLink>
        </div>
        <div className="header-account-links d-lg-none">
          <button
            type="button"
            className="main-menu-button btn btn-hidden fa fa-bars"
            label="menu"
            onClick={() => setMenuShown(true)}
          />
        </div>
      </div>
      <HeaderMainMenu
        items={data.links}
        shown={menuShown}
        setShown={setMenuShown}
        signinUrl={mlreefUrl}
      />
    </header>
  );
};

export default Header;
