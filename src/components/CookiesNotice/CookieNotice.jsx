import React, { useEffect, useState } from 'react';
import './CookieNotice.scss';

const CookieNotice = () => {
  const [accepted, setAccepted] = useState(true);

  const setAcceptFromCookie = status => {
    if (document) {
      document.cookie = `gdprAccepted=${status}; max-age=60; path=/`;
    }
  };

  const getAcceptFromCookie = () => {
    if (!document) return null;

    const value = document.cookie
      .split(';')
      .map(s => s.split('=').map(str => str.trim()))
      .filter(i => i[0] === 'gdprAccepted')
      .map(([k, v]) => ({ [k]: v }));

    return value.length && value[0].gdprAccepted;
  };

  useEffect(() => {
    setTimeout(() => {
      const v = getAcceptFromCookie();
      setAccepted(!!v);
    }, 1000);
  }, []);

  const handleAccept = () => {
    setAcceptFromCookie(true);
    setAccepted(true);
  };

  return (
    <div className="cookie-notice-positioner">
      <div className={`cookie-notice ${!accepted ? 'show' : ''}`}>
        <div className="cookie-notice-container">
          <div className="cookie-notice-content">
            <div className="cookie-notice-content-logo" />
            <div className="cookie-notice-content-message">
              <div className="title">This website uses cookies.</div>
              <p className="text">We use cookies to personalize content and ads, to provide social media features and to analyze our traffic. You consent to our cookies if you continue to use our website.</p>
            </div>
          </div>
          <div className="cookie-notice-actions">
            <div className="btn-group mr-3 d-none d-lg-flex">
              <button type="button" className="btn btn-basic-secondary btn-sm px-2">
                Necessary
              </button>
              <button type="button" className="btn btn-basic-secondary btn-sm px-2">
                Functional
              </button>
              <button type="button" className="btn btn-basic-secondary btn-sm px-2">
                Performance
              </button>
              <button type="button" className="btn btn-basic-secondary btn-sm px-2">
                Personalization
              </button>
              <button type="button" className="btn btn-secondary btn-sm px-2">
                Show details
              </button>
            </div>
            <button
              type="button"
              className="btn btn-info btn-label-sm btn-sm ml-auto"
              onClick={handleAccept}
            >
              OK
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CookieNotice;
