import React from 'react';
import { Link } from 'gatsby';

const MLink = props => {
  const {
    to,
    children,
    className,
    openApart,
  } = props;

  return (
    /^https?|^mailto/.test(to)
      ? (
        <a
          target={openApart ? '_blank' : null}
          href={to}
          className={className}
          rel={openApart ? 'noopener noreferrer' : null}
        >
          {children}
        </a>
      ) : (
        <Link
          to={to || '/'}
          className={className}
        >
          {children}
        </Link>
      )
  );
};

MLink.defaultProps = {
  className: '',
};

export default MLink;
