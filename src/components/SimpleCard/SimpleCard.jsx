import React from 'react';
import './SimpleCard.scss';

const SimpleCard = props => {
  const {
    title,
    text,
    subtext,
    className,
  } = props;

  return (
    <div className={`simple-card card ${className}`}>
      <div className="simple-card-container card-container">
        <div className="simple-card-title card-title">{title}</div>
        <div className="simple-card-text card-text">{text}</div>
        {subtext && (
          <div className="simple-card-subtext">{subtext}</div>
        )}
      </div>
    </div>
  );
};

SimpleCard.defaultProps = {
  className: '',
};

export default SimpleCard;
