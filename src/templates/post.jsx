import React from 'react';
import Layout from '../layout';
import './post.scss';

const PostTemplate = props => {
  const {
    pageContext: { html, frontmatter },
  } = props;

  return (
    <Layout title={frontmatter.title} className="post-template">
      <div
        className="post-template-top-banner-container"
        style={{ backgroundImage: `url(${frontmatter.image})` }}
      >
        <div className="cover">
          <div className="m-auto">
            <h1 className="post-template-top-banner-title">
              {frontmatter.title}
            </h1>
            <div>
              {`by ${frontmatter.author} // ${frontmatter.date}`}
            </div>
          </div>
        </div>
      </div>
      <div className="post-template-content">
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </div>
    </Layout>
  );
};

export default PostTemplate;
