import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../layout';
import PressCard from '../components/PressCard';
import './press.scss';

const PressPage = props => {
  const {
    data: {
      logos: { group: logos },
      additionals: { group: additionals },
      titles: { press: { titles } },
    },
  } = props;

  // console.log(props.data);

  return (
    <Layout title="Press" className="press-page simple-page">
      <div className="about-page-top-banner-container">
        <h1 className="about-page-top-banner-title">MLReef Press-kit</h1>
        <p className="about-page-top-banner-text">
          Official MLReef press kit including photos, logos, and our press release boilerplate.
        </p>
      </div>
      <div className="press-page-content simple-page-content">
        <section>
          <h2 className="">MLReef logos</h2>
          <p>PNG downloads have a transparent background, JPGs will have a white background.</p>

          <div className="press-page-content-container">
            {logos.map(resource => (
              <PressCard
                key={resource.fieldValue}
                resource={resource}
                titles={titles.logos}
              />
            ))}
          </div>
        </section>

        <section>
          <h2 className="">Additional images</h2>

          <div className="press-page-content-container">
            {additionals.map(resource => (
              <PressCard
                key={resource.fieldValue}
                resource={resource}
                titles={titles.additionals}
              />
            ))}
          </div>
        </section>

        <section className="mt-4 pt-3">
          <h2>Press release statement</h2>
          <p>Click the card to copy the text to your clipboard.</p>
          <div className="box border-rounded">
            <p>
              For AI to bring real added value, its development must be cost-effective.
              This economic perspective is crucial to make ML suitable and accessible
              to a wide range of users and applications.
            </p>
            <p>
              For MLReef to achieve this, we must remove the currently most essential
              limitations in ML development:
            </p>
            <ul>
              <li>
                <b>Lower the current entry barriers to ML</b>, through a flexible,
                holistic and structured ML development environment.
              </li>
              <li>
                <b>Promote network effects for deep collaboration</b> within a team
                and across an entire community to centralize and facilitate the exchange
                of technical and content-related ML elements (e.g. models, data and the like).
              </li>
              <li>
                <b>Increase work efficiency</b> with a focus on iterative, structured
                and embedded workflows to improve the quality of data and ML models.
              </li>
              <li>
                <b>Ensure full reproducibility</b> of the entire value chain to promote
                confidence and quality of the applicable ML model.
              </li>
            </ul>
            <p>
              These are our goals. MLReef is the globally first MLOps platform focussing
              on community collaboration. Our core tech allows instant reuse of any
              ML element published on MLReef. This drastically shortens development time
              and increases the model quality through fast and structured iterations.
            </p>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export const query = graphql`
  query FetchPressResources {
    logos: allFile(filter: {sourceInstanceName: {eq: "resources"}, relativeDirectory: {eq: "logos"}}) {
      group(field: name) {
        nodes {
          name
          publicURL
          extension
          relativePath
          internal {
            mediaType
          }
        }
        fieldValue
      }
      totalCount
    }

    additionals: allFile(filter: {sourceInstanceName: {eq: "resources"}, relativeDirectory: {eq: "additionals"}}) {
      group(field: name) {
        nodes {
          name
          publicURL
          extension
          relativePath
          internal {
            mediaType
          }
        }
        fieldValue
      }
      totalCount
    }

    fonts: allFile(filter: {sourceInstanceName: {eq: "resources"}, relativeDirectory: {eq: "fonts"}}) {
      group(field: name) {
        nodes {
          name
          publicURL
          extension
          relativePath
          internal {
            mediaType
          }
        }
        fieldValue
      }
      totalCount
    }

    titles: dataYaml {
      press {
        titles {
          additionals {
            resource
            text
          }
          logos {
            resource
            text
          }
        }
      }
    }
  }
`;

export default PressPage;
