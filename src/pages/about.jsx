import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../layout';
import MLink from '../components/MLink';
import './about.scss';
import TeamGallery from '../components/TeamGallery';

const getEmail = params => params.data && params.data.site.siteMetadata.email;

const AboutPage = props => (
  <Layout title="About us" className="about-page">
    <div className="about-page-top-banner-container">
      <h1 className="about-page-top-banner-title">About us</h1>
      <p className="about-page-top-banner-text">
        Learn more about us, where we came from and meet our core team
      </p>
    </div>
    <div className="about-page-content">
      <div className="">
        <h2>Our story</h2>
        <p>
          Our goal is to develop the most complete and satisfying <MLink to="/coming-soon">MLOps</MLink> platform.
          In the very center are our users as part of our interlinked community - therefore
          we are building MLReef as open source to allow collaboration on every aspect and to
          improve the general experience. We see MLReef as a public company as we share
          more information than most companies, meaning our projects, strategy and
          development progress are public and can be found within our website and
          our <MLink to="/coming-soon">team handbook</MLink>.
        </p>
        <p>
          MLReef believes in the democratization of Machine Learning. We believe
          that each individual stands at the center to empower the collective.
          In MLReef all user have access to all open ML content and are able to
          immediately use it - no boundaries and unlimited operational freedom.
        </p>

        <h2 className="mt-5">The company</h2>
        <p>
          MLReef was founded in 2019 and has ever since focused on developing the
          platform MLReef. MLReef is part of different consortia within academia
          and industry. Since its founding, MLReef is sponsored by NVIDIA, Amazon,
          ESA BIC, the Austrian government and the European Union.
        </p>

        <div className="about-page-banner card mt-4">
          <div className="about-page-banner-container card-container banner">
            <div
              style={{ backgroundImage: 'url(/images/MLReef_Logo_icon_01.png)' }}
              className="about-page-banner-image"
            />
            <div className="about-page-banner-content">
              <p>
                MLReef is an Austrian based company that <MLink to="/coming-soon">sells subscriptions</MLink> that offer
                more features and support. In addition, all services are built on AWS
                cloud computing, where we charge a 45% mark-up on the official <MLink to="/coming-soon">AWS S3 prices</MLink>.
              </p>
            </div>
          </div>
        </div>

        <div className="about-page-banner dark card mt-4">
          <div className="about-page-banner-container card-container banner">
            <div className="">
              <div className="">
                Building a community based MLOps experience
              </div>
              <p>
                Discover the <MLink to="/coming-soon">product features</MLink> and see, how
                the <MLink to="/coming-soon">community</MLink> can power-up your Machine Learning project.
              </p>
            </div>
          </div>
        </div>

        <h2 className="mt-5">Meet the team</h2>
        <p>
          MLReef is built on the community and our core team members. MLReef is
          all remote, interdisciplinary, diverse and inclusive.
        </p>
      </div>
    </div>

    <TeamGallery />

    <section className="about-page-section contact">
      <div className="contact-title">Still have questions about MLReef?</div>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href={`mailto:${getEmail(props)}`}
        className="btn btn-primary mt-2"
      >
        Get in touch
      </a>
    </section>
  </Layout>
);

export const query = graphql`
  query FetchEmail {
    site {
      siteMetadata {
        email
      }
    }
  }
`;

export default AboutPage;
