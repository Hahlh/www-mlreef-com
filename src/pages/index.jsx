import React, { useState } from 'react';
import { graphql } from 'gatsby';
import './index.scss';
import { mapPosts } from '../misc/dataParsers';
import Layout from '../layout';
import MLink from '../components/MLink';
import Aquarium from '../components/Aquarium';
import SimpleCard from '../components/SimpleCard';
import HomeStatement from '../components/HomeStatement';
import HomeFeatures from '../components/HomeFeatures';
import HomeBlog from '../components/HomeBlog';
import Marketplace from '../components/Marketplace';
import ContentCards from '../components/ContentCards';
import SEO from '../components/SEO';

const HomePage = props => {
  const {
    data: {
      blog,
      dataYaml: { homepage: data },
    },
  } = props;

  const [subscriptionEmail, setSubscriptionEmail] = useState('');
  const [subscriptionSuccess, setSubscriptionSuccess] = useState(false);

  // console.log(data);

  const {
    topBanner,
    features,
    sponsors,
    contactBanner,
    examples,
    accountFeatures,
    subscriptionBanner,
  } = data;

  const handleSubscription = e => {
    e.preventDefault();

    fetch(subscriptionBanner.action, {
      method: 'POST',
      body: JSON.stringify({ EMAIL: subscriptionEmail }),
    })
      .then(() => {
        setSubscriptionEmail('');
        setSubscriptionSuccess(true);
        setTimeout(() => setSubscriptionSuccess(false), 2000);
      });
  };

  return (
    <Layout withWedges className="home-page">
      <SEO />
      <Aquarium className="home-page-top-banner bg-dark">
        <div className="home-page-top-banner-container">
          <h3 className="home-page-top-banner-title">{ topBanner.title }</h3>
          {topBanner.text.split('\n').map(line => (
            <p
              key={`topbanner-text-${line}`}
              className="home-page-top-banner-text"
            >
              {line}
            </p>
          ))}
        </div>
        <div className="home-page-top-banner-footer">
          <div className="home-page-top-banner-footer-container border-rounded">
            <div className="home-page-top-banner-footer-text">
              {topBanner.footer}
            </div>
          </div>
        </div>
      </Aquarium>

      <section className="home-page-section features">
        {features && features.map(fea => (
          <SimpleCard
            key={`feature-${fea.title}`}
            title={fea.title}
            text={fea.text}
            subtext={fea.subtext}
          />
        ))}
      </section>

      {sponsors && (
        <section className="home-page-section sponsors">
          <h5 className="sponsors-title">Supported and powered by</h5>
          <div className="sponsors-container">
            {sponsors.map(spon => (
              <div
                className="sponsor"
                key={`sponsor-${spon.logo}`}
                title={spon.title}
                style={{ backgroundImage: `url(${spon.logo})` }}
              >
                {spon.link && (
                  <a
                    href={spon.link}
                    className="sponsor-link"
                    target="_blank"
                    rel="noopener noreferrer"
                    label="go"
                  />
                )}
              </div>
            ))}
          </div>
        </section>
      )}

      {contactBanner && (
        <section className="home-page-section contact">
          <div className="contact-title">{contactBanner.title}</div>
          <p className="mt-4">{contactBanner.text}</p>
          <MLink openApart to={contactBanner.href} className="btn btn-basic-primary mt-2">
            {contactBanner.buttonLabel || 'Contact!'}
          </MLink>
        </section>
      )}

      <Marketplace />

      {examples && (
        <section className="home-page-section examples">
          <HomeStatement
            title="Your entire Machine Learning life-cycle in one platform"
            text="MLReef is globally the first platform aiming at making software engineers part of a data science value chain."
          />

          {[].map(example => (
            <HomeStatement
              key={`example-${example.title}`}
              title={example.title}
              text={example.text}
              desktopImage={example.desktopImage}
              images={example.images}
            />
          ))}
        </section>
      )}

      <ContentCards className="pb-4" />

      <HomeFeatures features={accountFeatures} />

      {subscriptionBanner && (
        <section className="home-page-section subscription">
          <div className="subscription-title">{subscriptionBanner.title}</div>
          {subscriptionBanner.text.split('\n').map(line => (
            <p
              key={`subscription-text-${line}`}
              className="subscription-text-line"
            >
              {line}
            </p>
          ))}
          <form
            action={subscriptionBanner.action}
            method="post"
            id="mc-embedded-subscribe-form"
            name="mc-embedded-subscribe-form"
            className="subscription-form d-flex"
            target="_blank"
            noValidate
          >
            <div className="subscription-form-group btn-group mx-auto t-center">
              <input
                type="email"
                name="EMAIL"
                className="subscription-form-control border-rounded-left"
                id="mce-EMAIL"
                placeholder={subscriptionBanner.placeholder}
                value={subscriptionEmail}
                onChange={e => setSubscriptionEmail(e.target.value)}
                required
              />
              <button type="submit" className="btn btn-primary px-3">
                {subscriptionBanner.buttonLabel}
              </button>
            </div>

          </form>
          <div className={`subscription-success ${subscriptionSuccess ? 'show' : ''}`}>
            {subscriptionBanner.thankMessage}
          </div>
        </section>
      )}

      {blog && blog.nodes.length && <HomeBlog posts={mapPosts(blog.nodes)} />}
    </Layout>
  );
};

export const query = graphql`
  query FetchHomePageData {
    blog: allFile(
      filter: {sourceInstanceName: {eq: "blog"}},
      sort: {fields: childMarkdownRemark___frontmatter___date, order: DESC}
    ) {
      nodes {
        post: childMarkdownRemark {
          frontmatter {
            author
            image
            title
          }
          excerpt
        }
      }
    }

    dataYaml {
      homepage {
        topBanner {
          footer
          text
          title
        }
        features {
          title
          text
          subtext
        }
        sponsors {
          logo
          title
          link
        }
        contactBanner {
          title
          text
          buttonLabel
          href
        }
        examples {
          title
          text
          desktopImage
          images
        }
        accountFeatures {
          title
          image
          text
        }
        subscriptionBanner {
          title
          text
          buttonLabel
          placeholder
          action
          thankMessage
        }
      }
    }
  }
 `;

export default HomePage;
