import React from 'react';
import './index.scss';
import { useStaticQuery, graphql } from 'gatsby';
import { Helmet } from 'react-helmet';
import Header from '../components/Header';
import Footer from '../components/Footer/Footer';
import WedgesBackground from '../components/WedgesBackground';
// import CookiesNotice from '../components/CookiesNotice';

const Layout = props => {
  const {
    children,
    withWedges,
    className,
    title,
  } = props;

  const {
    dataYaml: { WedgesBackground: { wedges } },
    site: { siteMetadata: { title: defaultTitle } },
  } = useStaticQuery(
    graphql`
    query LayoutData {
      dataYaml {
        WedgesBackground {
          wedges {
            align
            color
            top
            size
          }
        }
      }

      site {
        siteMetadata {
          title
        }
      }
    }
    `,
  );

  return (
    <div className="layout-container bg-light">
      <Helmet>
        <title>
          {title ? `${title} | MLReef` : defaultTitle}
        </title>
      </Helmet>
      <Header />
      <main className={className}>
        {children}
      </main>
      {withWedges && (
        <WedgesBackground className="d-none d-lg-block" wedges={wedges} />
      )}
      <Footer />
    </div>
  );
};

Layout.defaultProps = {
  className: '',
};

export default Layout;
