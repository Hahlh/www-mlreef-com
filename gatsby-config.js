/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: 'Machine Learning Ops | MLReef',
    url: 'https://about.mlreef.com',
    siteUrl: 'https://about.mlreef.com',
    description: 'MLReef - your entire Machine Learning life cycle in one platform.',
    keywords: 'machine learning, data operations, deep learning, MLOPs, artificial intelligence, machine learning pipeline, git, git lfs, classification, clustering, neural networks, computer vision, supervised learning, applied AI',
    logo: '/images/MLReef_Logo_POS_H-01.svg',
    email: 'hello@mlreef.com',
    mlreefUrl: 'https://mlreef.com/login',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-polyfill-io',
      options: {
        features: [
          'Array.prototype.map',
          'Array.prototype.includes',
          'Array.prototype.find',
          'Object.entries',
          'URL',
          'URLSearchParams',
          'fetch',
        ],
      },
    },
    'gatsby-plugin-sass',
    'gatsby-transformer-remark',
    {
      resolve: 'gatsby-transformer-yaml',
      options: {
        // typeName: 'Yaml', // a fixed string
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'data',
        path: './data/',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'resources',
        path: './content/resources/',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'blog',
        path: './content/blog/',
      },
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'MLReef',
        short_name: 'MLReef',
        start_url: '/',
        background_color: '#b2b2b2',
        theme_color: '#1D2B40',
        display: 'standalone',
        icon: 'static/images/MLReef_Logo_icon_01.png',
      },
    },
    'gatsby-plugin-sitemap',
  ],
};
