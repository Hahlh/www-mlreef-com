/* eslint-disable */
const React = require('react');

exports.onPreRenderHTML = context => {
  const {
    getHeadComponents,
    replaceHeadComponents,
    // getPostBodyComponents,
    // replacePostBodyComponents
  } = context;

  /////////// <header /> /////////////

  const cookiebotInit = (
    <script
      key="cookiebot-consent-popup"
      id="Cookiebot"
      src="https://consent.cookiebot.com/uc.js"
      data-cbid="49c447b8-3fc0-4c24-acf2-0bc517cd2207"
      data-blockingmode="auto"
      type="text/javascript"
    />
  );

  const googleAnalitics = (
    <>
      {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165914461-1" />
      <script dangerouslySetInnerHTML={{ __html: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-165914461-1');
          `
        }}
      />
    </>
  );

  const headComponents = getHeadComponents();
  headComponents.push(cookiebotInit);
  headComponents.push(googleAnalitics);
  replaceHeadComponents(headComponents);

  /////////// <body /> ///////////////

  // this is a page with the declaration text.
  const cookiebotPopup = (
    <script
      key="cookiebot-declaration"
      id="CookieDeclaration"
      src="https://consent.cookiebot.com/c33d76f3-0841-48de-b741-b8aa11b86b9c/cd.js"
      type="text/javascript" async
    />
  );
};
