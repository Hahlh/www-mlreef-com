![Build Status](https://gitlab.com/mlreef/www-mlreef-com/badges/master/build.svg)

---
This repository contains the source code of the official MLReef homepage www.mlreef.com.

As part of our value of being transparent we welcome feedback. Please make a
[merge request](https://gitlab.com/mlreef/www-mlreef-com/merge_requests) to
suggest improvements or add clarifications. Please use
[issues](https://gitlab.com/mlreef/www-mlreef-com/issues) to ask questions.

---

## To add resources to press page

Every picture or icon can be auto deployed as a resource in the press page just
adding it to `content/resources` in one sub directory *logos* or *additionals* for being auto displayed.

> For the moment only *logos* and *additionals* are available, more directories
> can be easily included by editing the React file `src/pages/press.jsx`.

Every image will be shown as a downloadable item and files with same name (and different extension) will be grouped.

In addition it's possible to add a title, for this you need to look at `data/config.yaml` the field **press.titles** where **resource** means to the filename
and **text** the title for the image.



Based on [Gatsby Advanced Starter](https://github.com/Vagr9K/gatsby-advanced-starter)
