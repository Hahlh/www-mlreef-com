MLReef Working Model
====================

Bias for Action
--------------------


Smallest Viable Change
--------------------
Have you ever reviewed a Merge Request which was 1500 Lines long, covered three different tickets and touched seven different classes,
changing various different behaviours?

Did you look at a specific diff line and thought: "Does this now fix the bug in the database or add a feature in the REST-API?"

Big changesets (or Merge Requests) tend to get exponentially more complicated, they are harder to understand, and worst of all - almost always -
they violate Separation of Concerns by lumping together more than one _thing_.

> **Thing**: A single feature or bug or other behaviour change.

Small changesets (or Merge Requests) on the other hand are:

* usually change only one _thing_
* are easy to understand
* are easy to review
* are easy to revert
* enable cherry picking
* support onboarding
* are better traceable
* get into production faster


**Merge (request) early, merge often**

The Smallest Viable Change is found to be useful in very particular scenarios. 

One such usecase can be:
1. The developer/user, while creating a merge request, can choose to squash only the commit history and not delete the source branch if they are ambiguous whether there maybe any future commits that can be pushed into this branch.
2. In this way the branch is merged and yet remains undeleted and dormant until any other commits are pushed.

This results in a much more clean and precise commit history thus improving the readability and coherency of the project.

> **NOTE:** The use case mentioned above is one such example to use this principle.


Respect Makers Schedule
--------------------

In his [2009 article](http://www.paulgraham.com/makersschedule.html) Paul Graham laid out the differences between a typical maker's or programmer's schedule and the schedule of a typical manager.

According to Graham, the typical manager works in units of 30 or 60 minutes and schedules his day accordingly. This type of scheduling has practical advantages and allows for covering multiple topics throughout the day.

Graham writes:

> But there's another way of using time that's common among people who make things, like programmers and writers. They generally prefer to use time in units of half a day at least. You can't write or program well in units of an hour. That's barely enough time to get started.
>
> When you're operating on the maker's schedule, meetings are a disaster. A single meeting can blow a whole afternoon, by breaking it into two pieces each too small to do anything hard in. Plus you have to remember to attend the meeting. […] For someone on the maker's schedule, having a meeting is like throwing an exception. It doesn't merely cause you to switch from one task to another; it changes the mode in which you work.

Each of the schedules works well on their own, but as soon as they are combined, invisible costs and drawbacks are incurred. Breaking a (half) day into two parts, can reduce the motivation and morale for even starting ambitious tasks. Tasks that - by definition - reach close to the limits of or are as big as the _makers_ capacity might not even be started on such days - or ever.

> Don't your spirits rise at the thought of having an entire day free to work, with no appointments at all? Well, that means your spirits are correspondingly depressed when you don't. […] A small decrease in morale is enough to kill them off.



Of course there is no one size fits all schedule, which is why we aim to respect everyone's scheduling needs and actively communicate them with each other.
