[[_TOC_]]


Merge Requests
====================

Creating the Merge Request
--------------------
### Title
`123 Add animated 404-error page to the MLReef website`

The title shall:
 * Include the ticket ID
 * Describe why this Merge Request exists
 * Finish the following sentence: If applied, this MR will ...
 * Short |<----  Using a Maximum Of 50 Characters  ---->|
 * Start with an uppercase letter
 * **NOT** describe what can be seen in the Git diff

Merge Requests at MLReef get squash-merged. When doing this, Gitlab uses the Merge Request Title as part of the commit message for the squashed commit;


### Description
If the title alone does not contain enough information, we use the description of the Merge Request to explain to the reviewers and other collaborateurs
what the Merge Request actually changes - in as many or few words as necessary.

This allows the reviewer to quickly grasp the Merge Request's context and provide higher quality reviews.

**Additionally:** The description shall contain references to _closed_ or _related_ tickets.

`closes: #17`

`relates to: #18, #19`

### Settings
 * **enable** `Delete source branch when merge request is accepted.`
 * **enable** `Squash commits when merge request is accepted.`



Review & Approval
--------------------
In order to maintain high quality standards we are peer reviewing and testing each others code.
We take pride and ownership of this process.


### Review
In the **frontend** project the following default settings are configured:
 * Code Review by a second frontend developer
 * Frontend Testing by @cpmlreef (or @rainerkern as substitute)

> **Note:** Every developer can change the aproval settings for each for every merge request individually as needed.
This includes adding additional approvals as well as removing some or all approvals

After the Merge Request has been submitted it needs to be reviewed and approved.

As soon as the _Author_ wants his Merge Request to be reviewed, they assing the appropriate
person or persons to the Merge Request to give their approval. Usually a Merge Request will need multiple approvals,
thus it can be assigned to multiple people.

As a reviewer, if you are not satisfied with certain aspects, mention them in a comment.

If you have a question, assign the appropriate person.
This can mean to re-assign the original author to the Merge Request. 

Assigning one or more persons makes it explicit who has ownership of the Merge Request.

Assigning the author(s) to the Merge Request can be an indicator for them to do more work on the MR.

* Opening comment threads for source code lines effectively blocks an Merge Request from merging until the thread is resolved.
  This allows the Reviewer to approve the Merge Request, trusting in the author to take care of pending issues.

* As a reviewer, as soon as you are satisfied with the Merge Request, give your approval by clicking the _Approve_ button
in Gitlab's Merge Request overview page.

As long as there are other approvals needed **and** there are still other people assigned to the Merge Request,
after giving you approval, unassign (remove) yourself from the Merge Request.

The **last assignee** of the Merge Request carries the responsibility of the Merge Request beeing merged or reassigned.


Merging
----------------------
Unfortunately, Gitlab (inconsistently) somtimes takes the last commit message in a Merge Request and makes it the Squash-Message. This means, the person who merges a Merge Request is again responsible for putting the Merge Request Title as the _Squash Commit Message_ into the Merge Request.

![image](merge-request-sop-message.png)

From gitlab docs:
```
The squashed commit's commit message will be either:

Taken from the first multi-line commit message in the merge.
The merge request's title if no multi-line commit message is found.

It can be customized before merging a merge request.
```


Merge Request Template
----------------------
A merge request description should reflect your intention, not just the contents of the commits.
It is easy to see the changes in a commit, so the description should explain why you made those changes.
An example of a good description is: "Combine templates to reduce duplicate code in the user views."
The words "change," "improve," "fix," and "refactor" don't add much information to a merge request description.
For example, "Improve XML generation" could be better written as "Properly escape special characters in XML generation."

For now we want to establish the convention to start messages with uppercase letters and limit the length of the summary (the first line).

The following constraints are seen as "good" merge request description  and best practice:
- Start with an uppercase letter
- As a general idea, the git commit message describes what will happen if this commit is applied. Use present tense or imperative (which seem similar in English)
- Focus more on  WHAT or HOW this commit adds useful changes. WHAT is described in the diff itself and can already be seen.
- Limit the first line to 50 characters. provide a concise summary.
- If you want to provide more text, **insert an empty line after the first line** (summary), and use a bullet list to describe details or other interesting points

"If this merge request is applied, it will.." 
```markdown
Add fresh water to coffee machine and clean surface

relates to: #6

closes: #41

- a bullet list with interesting, but minor details
- make sure that water container is free of mold
```

For more information about formatting commit messages, please see this excellent [blog post by Tim Pope](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).


**Final note:** With our git-flow model commit messages can be seen as WIP and local anecdotes, until the merge request will be merged.

So as an idea: 
- use pretty, multi-line commits to describe the changes and keep those during merge requests
- use single-line commit messages for iterations of work
