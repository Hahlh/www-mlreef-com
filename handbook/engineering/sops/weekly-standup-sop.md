Weekly Development Standup(s)
====================

In order to coordinate our development efforts and equalize information throughout the, we are conducting two weekly team meetings with all developers. 

We believe that one of the pillars of this coordination is, that every team member has an idea about what everyone else in the team is doing.

Thus, during the Monday meeting, each team member describes his work objectives for the current week.

All weekly standups can be found in the [_Company Wiki_ project](https://gitlab.com/mlreef/company/issues?scope=all&utf8=%E2%9C%93&state=opened&search=Weekly+Standup)

@rainerkern is the owner of this meeting format.


### Format
The weekly standup will take place twice a week, at the beginning of the week and at the end of the week .
* During European Summer (European Daylight Savings time)
  * `Monday, 16:00 CET` and
  * `Thursday, 16:00 CET`
* During European Winter
  * `Monday, 15:00 CET` and
  * `Thursday, 15:00 CET`

The meeting will start sharp. After starting, Standups follow a semi random order per area of focus (product, epf, frontend, backend, infrastructure).

In order to conduct the meetings effectively and efficiently, we are always aiming to conduct them as asynchronously as possible.

**Preparation:** All team members write add their summaries as a comment to the issue **before** the start of the meetings.

* **Monday Standup:** Each participant has 2 - 3 minutes to describe their planned / open tickets for the current week and optional any peculiarities (challenge, input needed, timeline, required resources, etc).

* **End of Week Standup:** Each participant has 2 - 3 minutes to describe the progress of the listed tickets and point out, if necessary, any peculiarities encountered.
