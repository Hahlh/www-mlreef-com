# Introducing new Early Access users into MLReef

Introducing a new early alpha user is very important, as they are giving their 
time to test and commit to MLReef. As time will be limited, the most shall be 
taken out of their experience. 

>  MLReef is currently in alpha 1 status. This means, a lot does not work and there will be problems. The user needs to know and understand this!

1.  Set up a user account via manual CURL request on the develop environment.

2.  Send the user his log in credentials

3.  Invite user to the [slack community channel](mlreefcommunity.slack.com)

4.  Give them a personalized welcome message in Slack in channel "General" to break the ice

5.  Advice them on how to log into MLReef (login through mlreef.com) 

6.  Show him documentation link

7.  Send them the link to the early access survey: https://mlreef.typeform.com/to/X0QUt2

8.  Continuously follow up on troubles, survey, open issues, etc. 

