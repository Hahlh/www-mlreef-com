Create new Gitlab Project
====================

Standard Development Projects
--------------------
As a default, all projects have gitlab's merge approvals activated. This means that every merge request needs to be
looked over by at least one other developer.



Machine Learning Projects hosted in the www.gitlab.com/mlreef group
--------------------
> This is a temporary SOP and shall be deleted after creating projects in the MLReef GUI is possible

After Creating the project in gitlab

1. Add the Gitlab user `mlreefdemo` as developer to the project 
2. Activate "Snippets"
3. Increase CI/CD Timeout from 1h to 6h or more in the CI/CD Settings: 
4. Custom CI config path to .mlreef.yml in the CI/CD Settings
5. Disable the Gitlab Shared runners and only use the MLReef Group Runners in the CI/CD Settings: