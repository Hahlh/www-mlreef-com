At MLReef our handbook is extensive and keeping it relevant is an important part of everyone's job. The reasons for having a handbook are:

1. Reading is much faster than listening.
2. Reading is async, you don't have to interrupt someone or wait for them to become available.
3. Recruiting is easier if people can see what we stand for and how we operate.
4. Retention is better if people know what they are getting into before they join.
5. Onboarding is easier if you can find all relevant information spelled out.
6. Teamwork is easier if you can read how other parts of the company work.
7. Discussing changes is easier if you can read what the current process is.
8. Communicating change is easier if you can just point to a diff.
9. Everyone can contribute to it by proposing a change via a merge request.

Documenting things in the handbook takes more time initially and it requires thinking. But it saves time over a longer period and it is essential to scale and adapt our organization. It is not unlike writing tests for your software. Please follow these guidelines and remind others of them.

1. If you need to discuss with a team member for help please realize that probably the majority of the community also doesn't know, be sure to document the answer to radiate this information to the whole community. After the question is answered, discuss where it should be documented and who will do it. You can remind other people of this by asking "Who will document this?"

2. When you discuss something in chat always try to link to a URL where relevant, for example, the documentation you have a question about or the page that answered your question. You can remind other people of this by asking "Can you please link?"

3. To change a guideline or process, suggest an edit in the form of a merge request. After it is merged you can talk about it during the team call if applicable. You can remind other people of this by asking "Can you please send a merge request for the handbook?"

4. Communicate process changes by linking to the diff (a commit that shows the changes before and after). Do not change the process first, and then view the documentation as a lower priority task. Planning to do the documentation later inevitably leads to duplicate work communicating the change and to outdated documentation. You can remind other people of this by asking "Can you please update the handbook first?"

5. When communicating something always include a link to the relevant (and up to date) part of the handbook instead of including the text in the email/chat/etc. You can remind other people of this by asking "Can you please link to the relevant part of the handbook?"

6. If you copy content please remove it at the origin place and replace it with a link to the new content. Duplicate content leads to updating it in the wrong place, keep it DRY.

7. Make sure to always crosslink items if there are related items (elsewhere in the handbook, in docs, or in issues)

8. If someone inside or outside MLReef makes a good suggestion invite them to add it to the handbook. Send the person the url of the relevant page and section and offer to do it for them if they can't. Having them make and send it will make the change and review reflect their knowledge.

9. Learn how to edit the handbook using git. Please read through the Writing Style Guidelines before contributing.
Many things are documented in the handbook, but it will mostly be read by MLReefers.

If something concerns users of MLReef, it should be documented in the MLReef documentation, or the CONTRIBUTING file.
