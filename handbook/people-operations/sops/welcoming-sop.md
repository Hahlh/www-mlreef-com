Welcoming new team members
====================
The first day in a new team sets the tone for all the days that follow. At MLReef we are taking this very seriously.
We want to make new team members feel welcome and valued. We want to empower them to succeed - at MLReef and personally. 

Per default, the first working day of a new team member is always on Thursday.
This should give them just enough time to be able to participate in the _End of Week_ team meeting.
As an added benefit, it makes the first week of work relaxingly short.  

Beeing an all remote company this is not always easy and thus requires - aditionally to the overall team culture - some standardised steps.


Welcoming SOP
-------------------

1. For every new team member the line manager creates and assignes the *welcoming ticket*:

   ```markdown
   Preparation (before 1st day) 
   --------------------
   * [ ]  Finalize and sign contract
   * [ ]  Create Google User send login details
   * [ ]  Invite to MLReef Gitlab Group
   * [ ]  Invite to Slack Workspace
   * [ ]  Add to relevant email groups / lists
   * [ ]  Invite to the relevant developer standup(s)
   * [ ]  Add to relevant meetings
   * [ ]  Setup the "Welcome Call"
   * [ ]  Hand over this welcome ticket to the new team member.
   
  
   On your first day:
   ====================
   A warm welcome to MLReef, we are glad to have you on board.

   We warmly welcome you and will help you to make the transition as quick as possible.
   Since this is your first day as a full member of the team, let us get started with some orientation at first.
   
   > Note You can check off the tasks directly here in the Gitlab ticket

   * [ ]  Read the employee handbook
   * [ ]  Create your MLReef internal team profile in the ["company" repository](https://gitlab.com/mlreef/company/tree/master/Docs/People%20Operations/profiles) 
   * [ ]  Add your personal avatar to your Slack account
   * [ ]  Add your personal avatar to Gitlab account
   * [ ]  Add your personal avatar to your google account
   * [ ]  Suggest a change to the Code of Conduct or some other handbook page via Merge Request
   * [ ]  Suggest a change to the documentation (especially onboarding documentation) of at least one project
   * [ ]  Install the [Nod Reactions Chrome browser extension for google meet](https://chrome.google.com/webstore/detail/nod-reactions-for-google/oikgofeboedgfkaacpfepbfmgdalabej)
   
   ### Additional Tasks
   * [ ]  Read about [Gitlab's integrated todo list](https://docs.gitlab.com/ee/user/todos.html) it is very useful to stay on top of things
   * In order to get to know your colleagues better, we want you to also have good old fashioned coffee breakes with them.
     To get you used to the concept, we also added it as 10 mandatory steps to your introduction to MLReef
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
     * [ ]  Have a virtual coffee break with one of your colleagues
   
   
   Additional Notes
   ====================

   ### MLReef internal team profile

   Information that team profiles contain

   * Photo or Avatar
   * Name and optionally personal data (gender, birthday, etc.)
   * Contact data
     * company email address (private is optional)
     * company phone number (private is optional)
     * business address (private is optional)
   * Description about you - whatever you want to share

   Read more on the [MLReef handbook page on welcoming](https://gitlab.com/mlreef/www-mlreef-com/blob/master/handbook/people-operations/sops/welcoming-sop.md)
   
   ### Avatar

   Avatars or profile pictures are the pictures used in your online profiles.
   This can be a photo of you or any other picture. Many people also like to create 
   their own avatars on pages like [faceyourmanga.com](https://faceyourmanga.com/) or the [bitmoji app}(https://www.bitmoji.com/)
   ```

   Initially the ticket is assigned to the line manager. As soon as all preparation steps are performed, the ticket is assigned to the new team member.

2. Schedule the "What do you personally want to achieve" conversation within the first month and record the results
3. Schedule the "What does succeeding look like" conversation for the beginning of the second month and record the results
4. Schedule a follow up for the "What does succeeding look like" conversation in the fourth month


#### Contents for the Welcoming Email
The welcoming contains at least the following information:
* The login crednetials for the new teammembers google account
* A link to his welcoming ticket 

> Good Morning,
>
> first of all, a warm welcome to the team from my side.
>
> I am very happy that you are joining us.
> 
> ##### Your first day:
> To make your first day as smooth as possible we have prepared a Gitlab ticket for you
> which you can reach here -> _$TICKET_LINK_
> 
> Also, you should already have received the invitation for your welcoming call.
> 
> If you have any questions just hit reply :)
>
> ##### Your MLReef Google Account:
> Sign in to your Google Account to access the Google services that MLReef provides.
> Our G Suite, Google services include business-grade versions of Google Drive, Gmail
> and other Google services that you can use to collaborate with the team. 
> 
> Your username: _$USER_NAME_
>
> Password: _$USER_NAME_
>
> Sign in via this link: https://gsuite.google.com/dashboard
>
> If you have any questions in the meantime, just hit reply :)


### What do you personally want to achieve conversation

Every human has goals and wants to achieve something in his work life as well as in his private life. The idea and goal of this conversation is to understand the motivations and goals of our team members and make sure that we can help achieve them. We want to have stable and honest relationships between the individual team member and the company. Even if this should mean that we cannot work together at this time.


### What does succeeding look like conversation

The main goal of the "What does succeeding look like" conversation is to ensure that the new team member knows how to best achieve his job and how to best contribute to MLReef. It is not so much a test but rather a checkpoint in the welcoming process. In this conversation the new team member shall be able to articulate when and how he would label his work a a sucess and what he needs to achieve success.





### Greetbot Welcoming Messages

#### Hello @new_user!

Good to have you as new member at MLReef, please make yourself at home :sunglasses:
We are happy to welcome you as part of our team.

If you have any questions you can ask directly in the #general channel. It would also be a very nice Idea to introduce yourself to the other members of the team. They surely would appreciate to know who you are, where you are from, and what you like to do :slightly_smiling_face:

If you have never used slack before, you might want to watch this nice introduction video which will give you a good overview over the app.
https://www.youtube.com/watch?v=9RJZMSsH7-g

:spock-hand: Live long and Prosper :spock-hand:

#### Day — sent after 24 hours:

Hello again @new_user_name!
Hope you had a great first day and are starting to feel at home already :relaxed:.
For this day I have a very important question for you. You can send the answer either to me directly `Rainer`, or to our CEO `Camillo`.

The question is: What do you _personally_ want to achieve in the next 6 to 24 Months? *Not* at MLReef but rather in your personal life. Do you want to become an awesome developer and learn as much as you can? Do you want to get freelancing clients to earn a lot of money? Do you want to start you own startup eventually and want to see if the “startup life” is for you? Even if you just want to use MLReef as a stepping stone to a better job with less stress and higher pay - we want to be the best stepping stone there is.

I promise you, I will do my best to help you achieve your goal.

So lets have that conversation !

Cheers,
Rainer
And may the Force be with you

#### Week — sent after 7 days:

Hey @new_user_name!,
I hope you have been settling in fine here at MLReef. Since you are here now for a week maybe you have some questions for us. Please feel comfortable to ask them in the #general channel.

Even if you don’t have any questions - just between you and me - please send me some  feedback directly @Rainer Kern. I want to know how your first week was, if you were stressed or if you felt lost at times :sob: or if everything was great :sunglasses:
If there are any issues just let me know.
Best regards
Rainer

#### Month — sent after 28 days:

Hello @new_user_name! :relaxed:
It’s been a month since you’ve joined us here :spiral_calendar_pad: at MLReef. Thanks for being such a rockstar :the_horns: … so you are still here which is very very nice. Apparently you like it :wink:

Since you have been here for a Month, there is one thing we want to make sure of, which is that you know how to best contribute to MLReef and how your success is defined. Please be so kind and send me (@Rainer Kern) or @Camillo a message, articulating what you think this looks like for you and your position and - if you can also what you need to do that.

I am looking forward to hearing from you
Rainer

ps: If you want to see these wecloming messages again, you can just send a message with `greet` to @greetbot :hugging_face:
