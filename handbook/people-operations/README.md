People Operations
====================

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out pages related to People Operations in the next section below. If you can't find what you're looking for please do the following:


Role of People Operations
--------------------
In general, people operations is a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to MLReef. On that note, please don't hesitate to reach out with questions!

* [finding new team members](sops/qualification-sop.md)
* [welcoming new team members](sops/welcoming-sop.md)


### Invoicing and Taxes
All team members who are not employed directly by [MLReef GmbH](mlreef.com/imprint) in Austria are employed as freelancers.
This means that, technically, they are separat companies, usually in their country of residence.

> **Disclaimer:** The following is not legal advice in any way shape or form.

#### Austria
To setup a one person company "Einzelunternehmen" one has to:
* Get a "Gewerbeberechtigung" at your local municipal office (Bezirksamt or Gemeindeamt)
* Send invoices from your company to MLReef GmbH
  * If you earn less than ~35.000€ / year you can be freed from VAT ([Umsatzsteuer](https://www.wko.at/service/steuern/Umsatzsteuer_Ueberblick_in_Tabellenform.html))
  * If you earn more than 35.000€ / year you have to invoice 20% VAT
* At end of year add your earnings to your income tax report "Einkommenssteuer"

more information can be found at the [Austrian Chamber of Commerce (WKO)](https://www.wko.at/service/wirtschaftsrecht-gewerberecht/Einzelunternehmen.html)

#### Colombia
If a person is working as independent worker then he/she has to pay mandatory:
  * Social security services for pension, health care and proffessional risks, this [page helps with that](https://www.nuevosoi.com.co/inicio)
  * Erning more than 3.716.916 COP you have to pay a tax called "Renta" -> [this page has more information](https://www.finanzaspersonales.co/impuestos/articulo/declarar-renta-cuando-debo-declarar-renta-en-2018/74218)


Headquaters and Field Offices
--------------------
MLReef does neither have an official headquater, nor official offices.

Nevertheless, there are [various locations](locations.md)
