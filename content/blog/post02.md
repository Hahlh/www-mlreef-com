---
title: 'FirstDive: Chatbot'
author: Erika Torres
date: 2020-05-14
image: https://image.freepik.com/free-photo/paper-craft-art-speech-bubble-icon_53876-75081.jpg
---

## What can be considered a chatbot?
A chatbot is a NLP system that simulates a conversation within a limited context.

## Types of chatbots

There are three main types of chatbots in the industry:


### Rule based chatbots
This type of chatbot requires very specific instructions to function properly.
This means that you have to create a set of questions and answers to train the system,
the intention of each question must be very clear. For example: Do you support Windows 10?.

Intention based chatbots only can answer to commands, this means that each sentence
should have a clear intention. For example: Amazon’s Alexa, Google Home, Apple’s Siri, and Microsoft’s Cortana.

In order "to understand" the intents:

The system must identify the intention of the sentence and assign the meaning (a.k.a Intent)
so the machine can take action. We have to classify the types of intention
that will be supported by the chatbot.

The system identifies the intent using either keywords or text-based classification.
To use keywords, you have to associate words and phrases with intents, this approach
is limited but can be very effective.

To do text-based classification, we label a set of statements with the correspondent intents
and then train a classifier with this information. Then we add a proper response for each intent in the dataset.

We can train a classifier using a bag-of-words representation with a variety of algorithms,
from neural networks to clustering. If you are lucky enough to have lots of labeled data, another option for learning intents is to use deep learning, for example LSTM or RNN.


### Conversational chatbots
Conversational chatbots are more advanced than intention based chatbots, they can support a full conversation, theoretically. To do this, they must keep track of the state of the conversation and know when the person wants
to talk about something else, this is called the context.
