---
title: Why is ML not being used more?
author: Erika Torres
date: 2020-07-11
image: '/images/blog/empty_ML.jpg'
---

ML has been embraced by the biggest companies in the world, IBM, Google, Facebook , Amazon, Microsoft, Apple to mention some examples. We see wonderful results and applications of ML everyday on the news, billions of dollars on investments, thousands of scientists working on solving more problems. The global machine learning market is expected to grow to USD 8.81 Billion by 2022. 

However, I believe all the publicity around ML promotes a science fiction idea of ML, not so much the real applications and potential for all types of data problems. ML is not only deep learning or computer vision, there is a very wide variety of solutions for quotidienne problems. ML is often described as a very expensive tool for few companies that can afford to burn money into it. 

I think that all these far-fetched expectations are detrimental for the companies that are too afraid to waste time and money to try ML. The companies decide to add features to their products, if those present an opportunity to increase revenue or reduce costs. ML is not the first idea that comes to mind to achieve those goals, because the current practices to develop ML are making the process costly and unsustainable. Only 1 in 10 companies have deployed more than 10 AI solutions in production, chatbots, process optimization and fraud analysis are the top use cases, according to Forbes. In 2018’s Algorithmia’s survey* found that nearly 40% of the companies were starting to develop ML plans. The majority (55%) of companies surveyed have not deployed a machine learning model. 

The first obstacle is the developer community's lack of knowledge, to tackle this issue the big players have created services to “replace” the data scientist and offer ready-to-use models on demand, like AutoML by Google or AWS Marketplace. Those solutions are still not mature and it really does not solve the root problem, because you still require someone to clean the data and do the feature engineering. I would say that a better solution is to offer the developers a way to share their expertise and guide others in the process. We need a tool to reduce the friction of learning a new technology as Python did it in the past. 

The second obstacle is the unsustainability, the companies that build ML solutions found themselves in a never ending story, training and deploying constantly without getting the expected results. We need to optimize the ML lifecycle, that is a reality, most companies cannot afford years of development and continuous errors. The ML market has noticed those pitfalls and is investing heavily to fix those issues, machine learning platforms and applications cover half of all AI investments in 2019 at $42.9 billions**.
 
We need practical tools to change ML from a magical black box to a real affordable and sustainable solution. The ML industry invested huge amounts of resources to increase the penetration in the market approximately $82B last year **. However if we do not improve the ML ecosystem all this technology is going to be waste and lots of companies will lose the opportunity to narrow the gap.

> Source: Algorithmia, 2020 state of enterprise machine learning, Nov., 2019 (PDF, 29 PP., no opt-in).

> Source: Statista, Machine Learning Tops AI Dollars, May 10, 2019.


