---
title: Being all-remote
author: Camillo Pachmann
date: 2020-05-11
image: '/images/blog/being-all-remote.jpg'
---

When we founded MLReef, we wanted to create a truly global company. This meant for us having access to every talent on earth and pushing us to be diverse and open – in my view, this is one of the most beautiful aspects at MLReef.

After one year I can safely say: being all-remote is fantastic. We inspired the way we work much on our paragon [Gitlab](https://gitlab.com) and, I have to admit, we learned from the best. The most impact of being all-remote is on communication and coordination. We use Gitlab as our development platform and base much of our planning and communication in it. Text-based communication is great, as it forces you to write down your thoughts and progress. Nothing is ever lost, no ever repeating discussions, no lost information and it is highly efficient and reactionary. All great and pink, right?

Being a start-up means working with little resources to achieve the most. Being all-remote helps in this matter, a lot! Nevertheless, it also comes with its own challenges. MLReef´s main ingredient is the people behind it. We, humans, are very much used to social interaction to build trust and good working relationships. Digital-only communication bottlenecks interhuman relationships. The natural result is higher boundaries to call out for help, less direct feedback and an always resonant feel of isolation.

At MLReef we try to overcome this by having digital coffee breaks, allowing (very) informal chats before meetings, organizing our yearly DeepDive event and trying to meet in person whenever the possibilities arise. It forces all of us to try harder to be empathic and to be more open when we connect. It helps us to listen and to laugh louder. After this year, I would not want to miss a minute of being all-remote.
